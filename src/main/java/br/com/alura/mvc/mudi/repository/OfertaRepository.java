package br.com.alura.mvc.mudi.repository;

import br.com.alura.mvc.mudi.model.Oferta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OfertaRepository extends JpaRepository<Oferta, Long> {
}
