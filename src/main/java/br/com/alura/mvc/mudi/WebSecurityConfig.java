package br.com.alura.mvc.mudi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;
    Argon2PasswordEncoder encoder = new Argon2PasswordEncoder();

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/css/**", "/home", "/api/**", "/js/**", "/**.ico")
                .permitAll()
                .anyRequest().authenticated()
                .and().formLogin
                (form -> form.loginPage("/login")
                        .permitAll()
                        .defaultSuccessUrl("/usuario/pedido", true))
                .logout(logout -> logout.logoutUrl("/logout")
                        .logoutSuccessUrl("/home"))
                .csrf().disable();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(encoder)
                .usersByUsernameQuery("select username,password,enabled from spring_mvc.users where username = ?")
                .authoritiesByUsernameQuery("select username,authority from spring_mvc.authorities where username = ?");
    }


}