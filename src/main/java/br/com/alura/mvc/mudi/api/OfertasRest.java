package br.com.alura.mvc.mudi.api;

import br.com.alura.mvc.mudi.dto.RequisicaoNovaOferta;
import br.com.alura.mvc.mudi.model.Oferta;
import br.com.alura.mvc.mudi.model.Pedido;
import br.com.alura.mvc.mudi.repository.OfertaRepository;
import br.com.alura.mvc.mudi.repository.PedidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api/ofertas")
public class OfertasRest {

    private final PedidoRepository pedidoRepository;
    private final OfertaRepository ofertaRepository;

    @Autowired
    public OfertasRest(PedidoRepository pedidoRepository, OfertaRepository ofertaRepository) {
        this.pedidoRepository = pedidoRepository;
        this.ofertaRepository = ofertaRepository;
    }

    @PostMapping
    public Oferta criaOferta(@Valid @RequestBody RequisicaoNovaOferta requisicao) {
        System.out.println(requisicao);
        Optional<Pedido> pedidoBuscado = pedidoRepository.findById(requisicao.getPedidoID());
        if (!pedidoBuscado.isPresent())
            return null;
        Pedido pedido = pedidoBuscado.get();
        Oferta oferta = requisicao.toOferta();
        oferta.setPedido(pedido);
        ofertaRepository.save(oferta);
        return oferta;
    }
}
