package br.com.alura.mvc.mudi.repository;

import br.com.alura.mvc.mudi.dto.PedidoProjecao;
import br.com.alura.mvc.mudi.model.Pedido;
import br.com.alura.mvc.mudi.model.StatusPedido;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PedidoRepository extends JpaRepository<Pedido, Long> {

    List<Pedido> findByUserUsername(String username);

    List<Pedido> findByStatusPedidoAndUserUsername(StatusPedido statusPedido, String username);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"user"})
    List<PedidoProjecao> findByStatusPedido(StatusPedido statusPedido, Pageable pageable);
}