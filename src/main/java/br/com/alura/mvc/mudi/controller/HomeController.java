package br.com.alura.mvc.mudi.controller;

import br.com.alura.mvc.mudi.dto.PedidoProjecao;
import br.com.alura.mvc.mudi.model.Pedido;
import br.com.alura.mvc.mudi.model.StatusPedido;
import br.com.alura.mvc.mudi.repository.PedidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/home")
public class HomeController {

    private final PedidoRepository pedidoRepository;

    @Autowired
    public HomeController(PedidoRepository pedidoRepository) {
        this.pedidoRepository = pedidoRepository;
    }

    @GetMapping
    public String home(Model model, Principal principal) {
        Sort sort = Sort
                .sort(Pedido.class)
                .by(Pedido::getDataDaEntrega)
                .descending();
        PageRequest pageRequest = PageRequest.of(0, 2, sort);
        // List<Pedido> pedidos = pedidoRepository.findByStatusPedido(StatusPedido.ENTREGUE, pageRequest);
        List<PedidoProjecao> pedidos = pedidoRepository.findByStatusPedido(StatusPedido.ENTREGUE, pageRequest);

        model.addAttribute("pedidos", pedidos);
        return "home";
    }
}
