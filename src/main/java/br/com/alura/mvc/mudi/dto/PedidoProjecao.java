package br.com.alura.mvc.mudi.dto;

import br.com.alura.mvc.mudi.model.StatusPedido;
import org.springframework.beans.factory.annotation.Value;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface PedidoProjecao {
    Long getId();

    String getNomeDoProduto();

    BigDecimal getValorNegociado();

    LocalDate getDataDaEntrega();

    String getUrlProduto();

    String getUrlImagem();

    String getDescricao();

    StatusPedido getStatusPedido();

    @Value("#{target.user.username}")
    String getUsername();
}
