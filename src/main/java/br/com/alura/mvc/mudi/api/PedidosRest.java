package br.com.alura.mvc.mudi.api;

import br.com.alura.mvc.mudi.dto.PedidoProjecao;
import br.com.alura.mvc.mudi.model.Pedido;
import br.com.alura.mvc.mudi.model.StatusPedido;
import br.com.alura.mvc.mudi.repository.PedidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/pedidos")
public class PedidosRest {

    private final PedidoRepository pedidoRepository;

    @Autowired
    public PedidosRest(PedidoRepository pedidoRepository) {
        this.pedidoRepository = pedidoRepository;
    }

    @GetMapping("aguardando")
    public List<PedidoProjecao> getPedidosAguardandoOfertas() {
        Sort sort = Sort
                .sort(Pedido.class)
                .by(Pedido::getId)
                .ascending();
        PageRequest dezPedidosOrdemAsc = PageRequest.of(0, 10, sort);
        List<PedidoProjecao> pedidos = pedidoRepository.findByStatusPedido(StatusPedido.AGUARDANDO, dezPedidosOrdemAsc);
        return pedidos;
    }
}
