package br.com.alura.mvc.mudi.dto;

import br.com.alura.mvc.mudi.model.Oferta;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class RequisicaoNovaOferta {
    @NotNull
    private Long pedidoID;
    @NotNull
    @Pattern(regexp = "^\\d+(.\\d{2})?$")
    private String valor;
    @NotNull
    @Pattern(regexp = "^\\d{2}/\\d{2}/\\d{4}$")
    private String dataDaEntrega;
    private String comentario;

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public Long getPedidoID() {
        return pedidoID;
    }

    public void setPedidoID(Long pedidoID) {
        this.pedidoID = pedidoID;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getDataDaEntrega() {
        return dataDaEntrega;
    }

    public void setDataDaEntrega(String dataDaEntrega) {
        this.dataDaEntrega = dataDaEntrega;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Oferta toOferta() {
        Oferta oferta = new Oferta();
        oferta.setComentario(comentario);
        oferta.setDataDaEntrega(LocalDate.parse(dataDaEntrega, formatter));
        oferta.setValor(new BigDecimal(valor));
        return oferta;
    }

    @Override
    public String toString() {
        return "RequisicaoNovaOferta{" +
                "pedidoID=" + pedidoID +
                ", valor='" + valor + '\'' +
                ", dataDaEntrega='" + dataDaEntrega + '\'' +
                ", comentario='" + comentario + '\'' +
                '}';
    }
}
